# Maintainer:levinit <levinit at outlook dot com>  https://github.com/levinit/aur-pkgbuilds
# Maintainer: ValHue <vhuelamo at gmail dot com> https://github.com/ValHue/AUR-PKGBUILDs
# Ex-Maintainer: end222 <pabloorduna98 at gmail dot com>

pkgname=nautilus-megasync
pkgver=4.5.3.0
pkgrel=2
pkgdesc="Upload your files to your Mega account from nautilus."
arch=('i686' 'x86_64')
url="https://github.com/meganz/MEGAsync"
license=('custom:The Clarified Artistic License')
makedepends=('git' 'libnautilus-extension' 'qt5-tools' 'imagemagick')
conflicts=("${pkgname}-git")
_extname="_Win"
source=("git+https://github.com/meganz/MEGAsync.git#tag=v${pkgver}${_extname}"
        "https://metainfo.manjariando.com.br/megasync/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/filemanager.png")
sha256sums=('SKIP'
            '7c9c522c72a849faef929bade7ea12842923069a9fec8f511fa1ebdd8e6bbfb1'
            '3b19be9e841750dd69bc8bedb141dc74a9cac7ec6a885223cc5d1bf26af6d925')

_megasync_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_megasync_desktop" | tee com.${pkgname/-/_}.desktop

    cd "${srcdir}/MEGAsync/src/MEGAShellExtNautilus"
    qmake-qt5 MEGAShellExtNautilus.pro
    make
}

package() {
    depends=('libnautilus-extension' "megasync>=${pkgver%.*}")

    cd "${srcdir}/MEGAsync/src/MEGAShellExtNautilus"
    install -dm 755 ${pkgdir}/usr/lib/nautilus/extensions-3.0/
    install -m 755 -p libMEGAShellExtNautilus.so.1.0.0 ${pkgdir}/usr/lib/nautilus/extensions-3.0/

    cd "data/emblems"
    for size in 32x32 64x64
    do
        install -dm 755 ${pkgdir}/usr/share/icons/hicolor/${size}/emblems
        install -m 644 -p ${size}/mega-{pending,synced,syncing,upload}.{icon,png} ${pkgdir}/usr/share/icons/hicolor/${size}/emblems/
    done

    cd "${pkgdir}/usr/lib/nautilus/extensions-3.0"
    ln -f -s libMEGAShellExtNautilus.so.1.0.0 libMEGAShellExtNautilus.so.1.0
    ln -f -s libMEGAShellExtNautilus.so.1.0.0 libMEGAShellExtNautilus.so.1
    ln -f -s libMEGAShellExtNautilus.so.1.0.0 libMEGAShellExtNautilus.so
        
    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/filemanager.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
